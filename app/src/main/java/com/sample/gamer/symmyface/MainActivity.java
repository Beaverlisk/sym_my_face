package com.sample.gamer.symmyface;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.face.Face;
import com.sample.gamer.symmyface.async.AsyncTaskResult;
import com.sample.gamer.symmyface.bitmap.BitmapHandler;
import com.sample.gamer.symmyface.bitmap.MirrorBitmapType;
import com.sample.gamer.symmyface.tools.FaceLandmarksDetector;
import com.sample.gamer.symmyface.tools.FaceNotFoundException;
import com.sample.gamer.symmyface.tools.MirroringHandler;
import com.sample.gamer.symmyface.viewpager.PhotoFragmentPagerAdapter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends FragmentActivity {

    private static final int ACTION_LOAD_PHOTO = 1;
    private static final int ACTION_TAKE_PHOTO = 2;

    private ProgressBar progressBar;
    private TextView progressBarTextView;
    private ViewGroup progressBarLayout;
    private ViewPager viewPager;
    private PhotoFragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    private void initViews() {
        initViewPager();
        initViewPagerDots();
        initBottomNavigationView();
        initProgressBar();
    }

    private void initProgressBar() {
        progressBarLayout = (ViewGroup) findViewById(R.id.progress_bar_layout);
        progressBarTextView = (TextView) findViewById(R.id.tv_progress_bar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        adapter = new PhotoFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    private void initViewPagerDots() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_view_pager_dots);
        tabLayout.setupWithViewPager(viewPager, true);
    }

    private void initBottomNavigationView() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        setOnItemSelectedListener(bottomNavigationView);
    }

    private void setOnItemSelectedListener(BottomNavigationView navigationView) {
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.bottombaritem_take_photo:
                        takePhoto();
                        break;
                    case R.id.bottombaritem_insert_photo:
                        loadPhoto();
                        break;
                    case R.id.bottombaritem_mirror_photo:
                        mirrorPhoto();
                        break;
                    case R.id.bottombaritem_save_result_photo:
                        break;
                }
                return true;
            }
        });
    }

    public void takePhoto() {
        dispatchIntent(ACTION_TAKE_PHOTO);
    }

    public void loadPhoto() {
        dispatchIntent(ACTION_LOAD_PHOTO);
    }

    private void dispatchIntent(int actionCode) {
        Intent intent = new Intent();
        switch (actionCode) {
            case ACTION_LOAD_PHOTO:
                intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                if (intent.resolveActivity(getPackageManager()) == null) {
                    return;
                }
                break;
            case ACTION_TAKE_PHOTO:
                Toast.makeText(this, "Will be added soon", Toast.LENGTH_SHORT).show();
                //TODO: implement camera handling
//                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File pictureDir = getAlbumStorageDir(getApplicationContext(), getAlbumName());
//                File imageFile = new File(pictureDir, MirrorBitmapType.ORIGINAL.getImageFileName());
//                Uri pictureUri = Uri.fromFile(imageFile);
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
                return;
        }

        startActivityForResult(intent, actionCode);
    }

    public void mirrorPhoto() {
        new MirrorPhotoTask(MainActivity.this).execute();
    }

    private class MirrorPhotoTask extends AsyncTask<Void, Integer, AsyncTaskResult<List<String>>> {
        private Context context;
        private long lastUpdatedProgressTextTime = System.currentTimeMillis();

        public MirrorPhotoTask(Context context) {
            this.context = context;
        }

        @Override
        protected AsyncTaskResult<List<String>> doInBackground(Void... params) {
            List<String> mirroredBitmapsNames = new ArrayList<>(3);
            Bitmap originalPhotoBitmap = null;
            MirroringHandler symmetryHandler;
            try {
                originalPhotoBitmap = BitmapHandler.decodeFullSizeBitmap(getApplicationContext(), MirrorBitmapType.ORIGINAL.getImageFileName());
            } catch (FileNotFoundException e) {
                return new AsyncTaskResult<>(e);

            }
            publishProgress(40);
            FaceLandmarksDetector detector = new FaceLandmarksDetector(getApplicationContext(), originalPhotoBitmap);
            try {
                Face currentFace = detector.detectFace();
                symmetryHandler = new MirroringHandler(originalPhotoBitmap, detector.detectLandmark(currentFace));
            } catch (FaceNotFoundException e) {
                return new AsyncTaskResult<>(e);
            }
            publishProgress(60);
            try {
                String fileName = BitmapHandler.savePhotoBitmapToInternalStorage(getApplicationContext(), symmetryHandler.makeMirroredPhoto(MirrorBitmapType.LEFT), MirrorBitmapType.LEFT);
                mirroredBitmapsNames.add(0, fileName);
            } catch (IOException e) {
                return new AsyncTaskResult<>(e);
            }
            mirroredBitmapsNames.add(1, MirrorBitmapType.ORIGINAL.getImageFileName());
            publishProgress(80);
            try {
                String fileName = BitmapHandler.savePhotoBitmapToInternalStorage(getApplicationContext(), symmetryHandler.makeMirroredPhoto(MirrorBitmapType.RIGHT), MirrorBitmapType.RIGHT);
                mirroredBitmapsNames.add(2, fileName);
            } catch (IOException e) {
                return new AsyncTaskResult<>(e);
            }
            publishProgress(95);
            return new AsyncTaskResult<>(mirroredBitmapsNames);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onProgressUpdate(Integer... values) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastUpdatedProgressTextTime > 1200) {
                String[] arrayOfStrings = getApplicationContext().getResources().getStringArray(R.array.progress_bar_strings);
                String randomString = arrayOfStrings[new Random().nextInt(arrayOfStrings.length)];
                progressBarTextView.setText(randomString);
                lastUpdatedProgressTextTime = currentTime;
            }
            progressBar.setProgress(values[0], true);
        }

        @Override
        protected void onPreExecute() {
            progressBarLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(20);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(AsyncTaskResult<List<String>> result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            progressBarLayout.setVisibility(View.GONE);
            if (result.isSucceed()) {
                List<String> mirroredBitmapsNames = result.getData();
                adapter.setData(mirroredBitmapsNames);
            } else {
                Exception exception = result.getException();
                showError(context, exception);
            }
        }
    }

    private void showError(Context context, Exception exception) {
        String message;
        if (exception instanceof FileNotFoundException) {
            message = getString(R.string.msg_img_not_found);
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.show();
        } else if (exception instanceof FaceNotFoundException) {
            message = getString(R.string.msg_face_not_found);
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private void displayOriginalPhotoBitmap() {
        List<String> bitmapsNames = new ArrayList<>();
        bitmapsNames.add(MirrorBitmapType.ORIGINAL.getImageFileName());
        adapter.setData(bitmapsNames);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTION_LOAD_PHOTO:
            default:
                if (resultCode == RESULT_OK) {
                    Uri photoUri = data.getData();
                    try {
                        Bitmap originalPhotoBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
                        BitmapHandler.savePhotoBitmapToInternalStorage(getApplicationContext(), originalPhotoBitmap, MirrorBitmapType.ORIGINAL);
                        displayOriginalPhotoBitmap();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case ACTION_TAKE_PHOTO: {
                //TODO: implement camera handling
//                if (resultCode == RESULT_OK) {
//                    displayOriginalPhotoBitmapFromCamera();
//                } else if (requestCode == 0) {
//                    Toast.makeText(MainActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
//                }
                break;
            }
        }
    }

}
