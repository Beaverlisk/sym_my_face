package com.sample.gamer.symmyface.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapHandler {

    public static Bitmap decodeFullSizeBitmap(Context context, String imageFileName) throws FileNotFoundException {
        return BitmapFactory.decodeStream(context.openFileInput(imageFileName));
    }

    public static Bitmap decodeSampleSizeBitmap(Context context, String imageFileName, int reqWidth, int reqHeight) throws FileNotFoundException {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.openFileInput(imageFileName), null, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(context.openFileInput(imageFileName), null, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static String savePhotoBitmapToInternalStorage(Context context, Bitmap bitmap, MirrorBitmapType type) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        FileOutputStream fileOutputStream = context.openFileOutput(type.getImageFileName(), context.MODE_PRIVATE);
        fileOutputStream.write(bytes.toByteArray());
        fileOutputStream.close();
        return type.getImageFileName();
    }

}
