package com.sample.gamer.symmyface.viewpager;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.sample.gamer.symmyface.bitmap.BitmapHandler;
import com.sample.gamer.symmyface.R;

import java.io.FileNotFoundException;

public class PhotoFragment extends Fragment {
    public final static String IMAGE_KEY = "image";

    public PhotoFragment() {
        super();
    }

    public static PhotoFragment newInstance(String bitmapName) {
        PhotoFragment photoFragment = new PhotoFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_KEY, bitmapName);
        photoFragment.setArguments(args);
        return photoFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_view, null);
        final ImageView imageView = (ImageView) view.findViewById(R.id.image_view_fragment);
        Bundle bundle = getArguments() != null ? getArguments() : savedInstanceState;
        if (bundle != null) {
            final String bitmapName = bundle.getString(IMAGE_KEY);
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                    int finalImageViewHeight = imageView.getMeasuredHeight();
                    int finalImageViewWidth = imageView.getMeasuredWidth();
                    Bitmap bitmap = null;
                    try {
                        bitmap = BitmapHandler.decodeSampleSizeBitmap(getActivity(), bitmapName, finalImageViewHeight, finalImageViewWidth);
                        imageView.setImageBitmap(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    return true;
                }
            });
        }
        return view;
    }
}
