package com.sample.gamer.symmyface.bitmap;

/**
 * Created by Gamer on 17.10.2017.
 */

public enum MirrorBitmapType {
    LEFT,
    ORIGINAL,
    RIGHT;

    public String getImageFileName() {
        switch (this) {
            case LEFT:
                return "leftSideImage";
            case ORIGINAL:
            default:
                return "originalImage";
            case RIGHT:
                return "rightSideImage";
        }
    }
}
