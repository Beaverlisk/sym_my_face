package com.sample.gamer.symmyface.async;

public class AsyncTaskResult<T> {
    private T resultData;
    private Exception exception;

    public AsyncTaskResult(T resultData) {
        this.resultData = resultData;
    }

    public AsyncTaskResult(Exception exception) {
        this.exception = exception;
    }

    public boolean isSucceed() {
        return exception == null;
    }

    public T getData() {
        return resultData;
    }

    public Exception getException() {
        return exception;
    }
}
