package com.sample.gamer.symmyface.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;


public class PhotoFragmentPagerAdapter extends FragmentStatePagerAdapter {
    List<String> bitmapsNames = new ArrayList<>();

    public PhotoFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setData(List<String> bitmapsNames) {
        this.bitmapsNames = bitmapsNames;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        String bitmapName = bitmapsNames.get(position);
        return PhotoFragment.newInstance(bitmapName);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return bitmapsNames.size();
    }

}


