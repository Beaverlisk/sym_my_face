package com.sample.gamer.symmyface.tools;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;

import com.sample.gamer.symmyface.bitmap.MirrorBitmapType;

public class MirroringHandler {

    private PointF pointF;
    private Bitmap photoBitmap;
    private Bitmap overlay;

    public MirroringHandler(Bitmap bitmap, PointF pointF) {
        this.photoBitmap = bitmap;
        this.pointF = pointF;
    }

    public Bitmap makeMirroredPhoto(MirrorBitmapType mirrorBitmapType) {
        switch (mirrorBitmapType) {
            case LEFT:
                return makeLeftSideMirrorPhoto();
            case RIGHT:
            default:
                return makeRightSideMirrorPhoto();
        }
    }

    private Bitmap makeLeftSideMirrorPhoto() {
        int cx = (int) pointF.x;
        // int cy = (int) pointF.y;
        Bitmap leftSideBitmap = Bitmap.createBitmap(photoBitmap, 0, 0, cx, photoBitmap.getHeight());
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);
        Bitmap rightSideBitmap = Bitmap.createBitmap(leftSideBitmap, 0, 0, leftSideBitmap.getWidth(), leftSideBitmap.getHeight(), matrix, true);
        overlay = combineImages(leftSideBitmap, rightSideBitmap);
        return overlay;
    }

    private Bitmap makeRightSideMirrorPhoto() {
        int cx = (int) pointF.x;
        // int cy = (int) pointF.y;
        Bitmap rightSideBitmap = Bitmap.createBitmap(photoBitmap, cx, 0, (photoBitmap.getWidth() - cx), photoBitmap.getHeight());
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);
        Bitmap leftSideBitmap = Bitmap.createBitmap(rightSideBitmap, 0, 0, rightSideBitmap.getWidth(), rightSideBitmap.getHeight(), matrix, true);
        overlay = combineImages(leftSideBitmap, rightSideBitmap);
        return overlay;
    }

    private Bitmap combineImages(Bitmap leftSideBitmap, Bitmap rightSideBitmap) {
        Bitmap combinedBitmap = null;
        int width, height = 0;
        if (leftSideBitmap.getWidth() > rightSideBitmap.getWidth()) {
            width = leftSideBitmap.getWidth() + rightSideBitmap.getWidth();
            height = leftSideBitmap.getHeight();
        } else {
            width = rightSideBitmap.getWidth() + rightSideBitmap.getWidth();
            height = leftSideBitmap.getHeight();
        }
        combinedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas comboImage = new Canvas(combinedBitmap);
        comboImage.drawBitmap(leftSideBitmap, 0f, 0f, null);
        comboImage.drawBitmap(rightSideBitmap, leftSideBitmap.getWidth(), 0f, null);
        return combinedBitmap;
    }


}
