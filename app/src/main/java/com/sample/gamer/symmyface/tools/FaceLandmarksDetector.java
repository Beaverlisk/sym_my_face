package com.sample.gamer.symmyface.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.Landmark;

public class FaceLandmarksDetector {
    private Context context;
    private Bitmap photoBitmap;

    public FaceLandmarksDetector(Context context, Bitmap bitmap) {
        this.context = context;
        this.photoBitmap = bitmap;
    }

    public Face detectFace() throws FaceNotFoundException {
        FaceDetector faceDetector = new FaceDetector.Builder(context)
                .setTrackingEnabled(false)
                .setLandmarkType(com.google.android.gms.vision.face.FaceDetector.ALL_LANDMARKS)
                .build();
        if (!faceDetector.isOperational()) {
            return null;
        }
        Frame frame = new Frame.Builder().setBitmap(photoBitmap).build();
        SparseArray<Face> faces = faceDetector.detect(frame);

        if (faces != null && faces.size() > 0) {
            Face currentFace = faces.valueAt(0);
            return currentFace;
        } else {
            throw new FaceNotFoundException();
        }
    }


    public PointF detectLandmark(Face currentFace) {
        if (currentFace != null)
            for (Landmark landmark : currentFace.getLandmarks()) {
                if (landmark.getType() == Landmark.NOSE_BASE) {
                    PointF pointFNoseBase = new PointF(landmark.getPosition().x, landmark.getPosition().y);
                    return pointFNoseBase;
                }
            }
        return null;
    }
}





